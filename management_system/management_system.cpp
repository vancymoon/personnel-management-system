//
// Created by 东明 on 2019-04-10.
//

#include "management_system.h"
#include <fstream>
#include <streambuf>

ERROR ManagementSystem::initFromFile(const char *filename) {
    std::ifstream t("pms.json");
    std::string raw_pms((std::istreambuf_iterator<char>(t)),
                        std::istreambuf_iterator<char>());

    lightjson::Json json;
    json.Parse(raw_pms);

    lightjson::Json::j_array json_array = json.ToArray();

    for (const auto &it: json_array) {
        insertPersonFromJson(it);
    }
}

ERROR ManagementSystem::insertPerson() {
    char input_type;
    std::cout << "Enter the person type\n"
                 "      u for undergraduate\n"
                 "      f for fulltime graduate\n"
                 "      o for onjob graduate\n"
                 "      s for staff\n"
                 "      t for teacher\n"
                 ">> ";
    std::cin >> input_type;
    bool input_type_status = false;
    while (!input_type_status) {
        switch (input_type) {
            case 'u': {
                Undergrad new_undergrad;
                new_undergrad.inputInfo();
                undergrads.push_back(new_undergrad);
                input_type_status = true;
                break;
            }
            case 'f': {
                FulltimeGrad new_fulltimegrad;
                new_fulltimegrad.inputInfo();
                fulltime_grads.push_back(new_fulltimegrad);
                input_type_status = true;
                break;
            }
            case 'o': {
                OnjobGrad new_onjobgrad;
                new_onjobgrad.inputInfo();
                onjob_grads.push_back(new_onjobgrad);
                input_type_status = true;
                break;
            }
            case 's': {
                Staff new_staff;
                new_staff.inputInfo();
                staff.push_back(new_staff);
                input_type_status = true;
                break;
            }
            case 't': {
                Teacher new_teacher;
                new_teacher.inputInfo();
                teachers.push_back(new_teacher);
                input_type_status = true;
                break;
            }
            default: {
                std::cout << "Invalid type, re-input: ";
            }
        }

    }

    return SUCCESS;
}

ERROR ManagementSystem::insertPersonFromJson(lightjson::Json json) {
    lightjson::Json::j_object jObject = json.ToObject();
    switch (static_cast<int>(jObject["type"].ToNumber())) {
        case 0: {
            Undergrad new_undergrad;
            new_undergrad.setName(jObject["name"].ToString());
            new_undergrad.setGender(static_cast<Gender >(jObject["type"].ToNumber()));
            new_undergrad.setAge(static_cast<int>(jObject["age"].ToNumber()));
            new_undergrad.setID(jObject["id"].ToString());
            new_undergrad.setExamScore(static_cast<int>(jObject["exam_score"].ToNumber()));
            undergrads.push_back(new_undergrad);
            break;
        }
    }

    return SUCCESS;
}

ERROR ManagementSystem::deletePerson() {
    std::string id;
    std::cout << "WARNING: you are deleting a person...\n"
                 "Enter ID: ";
    std::cin >> id;

    for (auto it = undergrads.begin(); it != undergrads.end(); ++it) {
        if (id == it->getID()) {
            undergrads.erase(it);
            return SUCCESS;
        }
    }

    for (auto it = fulltime_grads.begin(); it != fulltime_grads.end(); ++it) {
        if (id == it->getID()) {
            fulltime_grads.erase(it);
        }
        return SUCCESS;
    }

    for (auto it = onjob_grads.begin(); it != onjob_grads.end(); ++it) {
        if (id == it->getID()) {
            onjob_grads.erase(it);
        }
        return SUCCESS;
    }

    for (auto it = staff.begin(); it != staff.end(); ++it) {
        if (id == it->getID()) {
            staff.erase(it);
        }
        return SUCCESS;
    }

    for (auto it = teachers.begin(); it != teachers.end(); ++it) {
        if (id == it->getID()) {
            teachers.erase(it);
        }
        return SUCCESS;
    }

    return DELETE_FAIL;
}

ERROR ManagementSystem::searchPerson() {
    std::string id;
    unsigned long student_number = -1;
    std::string name;

    char search_by;
    std::cout << "Enter the way you want to search by:\n"
                 "      i for ID\n"
                 "      n for name\n"
                 "      s for student number\n";
    std::cin >> search_by;
    switch (search_by) {
        case 'i':
            std::cout << "Enter ID: ";
            std::cin >> id;
            break;
        case 'n':
            std::cout << "Enter name: ";
            getchar();
            std::getline(std::cin, name);
            break;
        case 's':
            std::cout << "Enter student number";
            std::cin >> student_number;
            break;
    }

    return searchPersonHelper(id, student_number, name);
}

ERROR ManagementSystem::searchPersonHelper(std::string id, unsigned long student_number, std::string name) {
    if (student_number != -1) {
        for (auto it = onjob_grads.begin(); it != onjob_grads.end(); ++it) {
            if (student_number == it->getStudentNumber()) {
                it->displayInfo();
            }
        }
    }

    for (auto &undergrad : undergrads) {
        if (id == undergrad.getID() || name == undergrad.getName()) {
            undergrad.displayInfo();
        }
    }

    for (auto &fulltime_grad : fulltime_grads) {
        if (id == fulltime_grad.getID() || name == fulltime_grad.getName()) {
            fulltime_grad.displayInfo();
        }
    }

    for (auto &onjob_grad : onjob_grads) {
        if (id == onjob_grad.getID() || name == onjob_grad.getName()) {
            onjob_grad.displayInfo();
        }
    }

    for (auto &it : staff) {
        if (id == it.getID() || name == it.getName()) {
            it.displayInfo();
        }
    }

    for (auto &teacher : teachers) {
        if (id == teacher.getID() || name == teacher.getName()) {
            teacher.displayInfo();
        }
    }

    return SUCCESS;
}