//
// Created by 东明 on 2019-04-10.
//

#ifndef CPP_HOMEWORK5_MANAGEMENT_SYSTEM_H
#define CPP_HOMEWORK5_MANAGEMENT_SYSTEM_H

#include <person.h>
#include <list>
#include <memory>
#include "json.h"

enum ERROR {
    SUCCESS,
    DELETE_FAIL
};

class ManagementSystem {
public:
    ERROR initFromFile(const char *filename);

    ERROR insertPerson();

    ERROR insertPersonFromJson(lightjson::Json json);

    ERROR deletePerson();

    ERROR searchPerson();

private:
    ERROR searchPersonHelper(std::string id, unsigned long student_number, std::string name);

    std::list<Undergrad> undergrads;
    std::list<FulltimeGrad> fulltime_grads;
    std::list<OnjobGrad> onjob_grads;
    std::list<Staff> staff;
    std::list<Teacher> teachers;

};


#endif //CPP_HOMEWORK5_MANAGEMENT_SYSTEM_H
