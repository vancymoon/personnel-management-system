#include <iostream>
#include <list>

#include <management_system.h>
#include <json.h>
#include <string>
#include <fstream>
#include <streambuf>


using namespace std;
using namespace lightjson;

int main() {

    ManagementSystem ms;
    ms.initFromFile("pms.json");

    ms.insertPerson();
    ms.deletePerson();
    ms.searchPerson();
    ms.searchPerson();
    return 0;
}