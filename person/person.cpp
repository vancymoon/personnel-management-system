//
// Created by 东明 on 2019-04-10.
//

#include "person.h"

void Person::inputInfo() {
    std::string name;
    std::cout << ">> Enter name: ";
    getchar();
    std::getline(std::cin, name);
    name_ = name;

    char gender;
    std::cout << ">> Enter genger, m(ale), f(emale) or u(ndefined): ";
    std::cin >> gender;
    switch (gender) {
        case 'm':
            gender_ = kmale;
            break;
        case 'f':
            gender_ = kfemale;
            break;
        case 'u':
            gender_ = kundefined;
            break;
    }

    short age;
    std::cout << ">> Enter age: ";
    std::cin >> age;
    age_ = age;

    std::string id;
    std::cout << ">> Enter ID: ";
    std::cin >> id;
    id_ = id;
}

void Person::displayInfo() {
    std::cout << "Name:  " << name_ << std::endl;
    std::cout << "Age:  " << age_ << std::endl;
    std::cout << "ID:  " << id_ << std::endl;
}