//
// Created by 东明 on 2019-04-10.
//

#ifndef CPP_HOMEWORK5_PERSON_H
#define CPP_HOMEWORK5_PERSON_H

#include <stdio.h>
#include <iostream>
#include <string>

enum PersonType {
    kundergrad,
    kfulltimegrad,
    konjobgrad,
    kstaff,
    kteacher
};

enum Gender :int {
    kmale, kfemale, kundefined
};

class Person {
public:
    virtual void inputInfo() = 0;

    virtual void displayInfo() = 0;

    std::string getID() {
        return id_;
    }

    std::string getName() {
        return name_;
    }

protected:
    PersonType type_;
    std::string name_;
    Gender gender_;
    short age_;
    std::string id_;
};

template<PersonType T>
class PersonWithType : public Person {
public:
    PersonWithType() {
        type_ = T;
    }
};

class Undergrad : public PersonWithType<kundergrad> {
public:
    void inputInfo() override {
        PersonWithType::inputInfo();
        int college_entrance_examination_score;
        std::cout << ">> Enter college entrance examination score: ";
        std::cin >> college_entrance_examination_score;
        college_entrance_examination_score_ = college_entrance_examination_score;
    }

    void displayInfo() override {
        Person::displayInfo();
        std::cout << "College entrance exam score:  " << college_entrance_examination_score_ << std::endl;
    }

    void setName(std::string name) {
        name_ = name;
    }

    void setGender(Gender gen) {
        gender_ = gen;
    }

    void setAge(int age) {
        age_ = age;
    }

    void setID(std::string id) {
        id_ = id;
    }

    void setExamScore(int score) {
        college_entrance_examination_score_ = score;
    }

private:
    int college_entrance_examination_score_;
};

class FulltimeGrad : public PersonWithType<kfulltimegrad> {
public:
    void inputInfo() override {
        PersonWithType::inputInfo();
        std::string specialty;
        std::cout << "Enter specialty: ";
        getchar();
        std::getline(std::cin, specialty);
        specialty_ = specialty;
    }

    void displayInfo() override {
        Person::displayInfo();
        std::cout << "Specialty:  " << specialty_ << std::endl;
    }

private:
    std::string specialty_;
};

class OnjobGrad : public PersonWithType<konjobgrad> {
public:
    void inputInfo() override {
        PersonWithType::inputInfo();
        std::string specialty;
        std::cout << "Enter specialty: ";
        getchar();
        std::getline(std::cin, specialty);
        specialty_ = specialty;

        unsigned long student_number;
        std::cout << "Enter student number: ";
        std::cin >> student_number;
        student_number_ = student_number;

        unsigned int salary;
        std::cout << "Enter salary: ";
        std::cin >> salary;
        salary_ = salary;
    }

    unsigned long getStudentNumber() {
        return student_number_;
    }

    void displayInfo() override {
        Person::displayInfo();
        std::cout << "Specialty:  " << specialty_ << std::endl;
        std::cout << "Salary:  " << salary_ << std::endl;

    }

private:
    std::string specialty_;
    unsigned long student_number_;
    unsigned int salary_;
};

class Staff : public PersonWithType<kstaff> {
public:
    void inputInfo() override {
        PersonWithType::inputInfo();
        std::string job;
        std::cout << "Enter job: ";
        std::cin >> job;
        job_ = job;

        unsigned int salary;
        std::cout << "Enter salary: ";
        std::cin >> salary;
        salary_ = salary;
    }

    void displayInfo() override {
        Person::displayInfo();
        std::cout << "Job:  " << job_ << std::endl;
        std::cout << "Salary:  " << salary_ << std::endl;

    }

private:

    std::string job_;
    unsigned int salary_;
};

class Teacher : public PersonWithType<kteacher> {
public:
    void inputInfo() override {
        PersonWithType::inputInfo();
        std::string specialty;
        std::cout << "Enter specialty: ";
        getchar();
        std::getline(std::cin, specialty);
        specialty_ = specialty;

        unsigned int salary;
        std::cout << "Enter salary: ";
        std::cin >> salary;
        salary_ = salary;
    }

    void displayInfo() override {
        Person::displayInfo();
        std::cout << "Specialty:  " << specialty_ << std::endl;
        std::cout << "Salary:  " << salary_ << std::endl;

    }

private:


    std::string specialty_;
    unsigned int salary_;
};

#endif //CPP_HOMEWORK5_PERSON_H
